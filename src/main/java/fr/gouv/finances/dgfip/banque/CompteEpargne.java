package fr.gouv.finances.dgfip.banque;

import java.util.ArrayList;
import java.util.List;

public class CompteEpargne extends CompteBancaire {

	private Double taux;

	List<Operation> listOperation = new ArrayList<Operation>();

	public CompteEpargne(String codeBanque, String codeGuichet, String numCompte, String cle, Double soldeInitial,
			Double taux) {
		this.codeBanque = codeBanque;
		this.codeGuichet = codeGuichet;
		this.taux = taux;
		this.solde = soldeInitial;

		// TODO: generate these fields

		this.numCompte = numCompte;
//		Random rand = new Random();
//		Integer nombreAleatoire = rand.nextInt(90) + 10;
//		cle = nombreAleatoire.toString();
		this.cle = cle;
	}

	public Double calculerSolde() {
		Double solde = this.solde;
		for (Operation listeOp : listOperation) {
			solde += listeOp.montant;
		}
		return solde;
	}

	public Integer creerOperation(String libelle, Double montant) throws CompteException {
		if (calculerSolde() + montant < 0) {
			throw new CompteException("Operation impossible : solde negatif");
		} else {
			return super.creerOperation(libelle, montant);
		}
	}
}
