package fr.gouv.finances.dgfip.banque;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Java
 * 
 * @author stagiaire1
 *
 */
public class BanqueV0Application {
	public static void main(String[] args) throws CompteException {
		// Le code à développer dans le cadre de ce TP doit permettre d'exécuter les cas
		// d'usage ci-dessous.
		CompteCourant cc = null;
		CompteEpargne ce = null;

		SimpleDateFormat formater = null;
		formater = new SimpleDateFormat("dd-MM-YYYY");

		Banque maBanque = new Banque("DGFIP");

//		Set<Personne> setPersonne = new HashSet<Personne>();
//		DonneesDAOPersonnes.creerNouveauTitulaire(setPersonne);

		System.out.println("*************************");
		System.out.println("*** Liste des clients ***");
		System.out.println("*************************\n");

		maBanque.afficherListeClients();

		System.out.println("*********************************");
		System.out.println("*** Liste des comptes ouverts ***");
		System.out.println("*********************************\n");

		maBanque.afficherListeComptesOuverts(maBanque);

		System.out.println("*******************************************");
		System.out.println("*** Situation des comptes au " + formater.format(new Date()) + " ***");
		System.out.println("*******************************************\n");

		maBanque.afficherSyntheseComptes();

		System.out.println("***************************");
		System.out.println("*** type des mouvements ***");
		System.out.println("***************************\n");

		maBanque.ajouterMouvement();

		System.out.println("****************************");
		System.out.println("*** Synthèse des comptes ***");
		System.out.println("****************************\n");

		maBanque.afficherSynthese();

		System.out.println("*******************");
		System.out.println("*** Création CB ***");
		System.out.println("*******************\n");

		maBanque.creerCarte();

//		System.out.println("*** Création CB Dominique                             ***");
//		CarteBancaire cbDominique = maBanque.creerCarte(dominique, ccDominique);
//		System.out.println("*********************************************************\n");
//		System.out.println("");

		System.out.println("***********************");
		System.out.println("*** Synthèse cartes ***");
		System.out.println("***********************\n");

		maBanque.afficherSyntheseCartes();

		// Gabier gabier1 = new Gabier(maBanque);
//
//		System.out.println("***********************************************************");
//		System.out.println("*** Retrait de 314 € sur le compte courant de Paulette  ***");
//		System.out.println("***********************************************************");
//		try {
//			List<String> comptesPaulette = gabier1.accesComptes(cbPaulette.getNumCarte(), cbPaulette.getCodePin());
//			System.out.println("*** Liste de comptes de Paulette:");
//			for (String rib : comptesPaulette) {
//				System.out.println("*** RIB -> " + rib);
//			}
//			int numOperation = gabier1.retirerEspeces(comptesPaulette.get(0), 314.0);
//			System.out.format("*** Opération réalisée: %d  ***", numOperation);
//			System.out.println("");
//			System.out.println("");
//			ccPaulette.afficherSyntheseOperations();
//		} catch (SystemeBancaireException e) {
//			System.err.println("Accès impossible aux comptes de Paulette: " + e.getMessage());
//		}
//		System.out.println("");
//
//		System.out.println("*************************************************************");
//		System.out.println("*** Tentative retrait de 567 € sur le compte de Dominique ***");
//		System.out.println("*************************************************************");
//		try {
//			List<String> comptesDominique = gabier1.accesComptes(cbDominique.getNumCarte(), "CODE PIN FAUX");
//			System.out.println("*** Liste de comptes de Dominique:");
//			for (String rib : comptesDominique) {
//				System.out.println("***  RIB -> " + rib);
//			}
//			int numOperation = gabier1.retirerEspeces(comptesDominique.get(0), 567.0);
//			System.out.format("*** Opération réalisée: %d  ***", numOperation);
//			ccDominique.afficherSyntheseOperations();
//		} catch (SystemeBancaireException e) {
//			System.err.println("Accès impossible aux comptes de Dominique: " + e.getMessage());
//			ccDominique.afficherSyntheseOperations();
//		}
	}

}