package fr.gouv.finances.dgfip.banque;

import java.util.ArrayList;
import java.util.List;

public class Personne {

	public String nom;
	public String prenom;

	private List<CompteBancaire> listeCompte = new ArrayList<CompteBancaire>();

	public Personne(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + "]";
	}

	public void addCompte(CompteBancaire c) {
		listeCompte.add(c);
	}

}
