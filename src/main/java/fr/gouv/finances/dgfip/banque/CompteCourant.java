package fr.gouv.finances.dgfip.banque;

import java.util.Random;

public class CompteCourant extends CompteBancaire {

	@Override
	public String toString() {
		return "CompteCourant [codeBanque=" + codeBanque + ", codeGuichet=" + codeGuichet + ", numCompte=" + numCompte
				+ ", cle=" + cle + ", solde=" + solde + ", getRib()=" + getRib() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	public CompteCourant(String codeBanque, String codeGuichet, Double soldeInitial, Personne titulaire) {
		this.codeBanque = codeBanque;
		this.codeGuichet = codeGuichet;
		this.solde = soldeInitial;
		this.titulaire = titulaire;

		// TODO: generate these fields
		Random rand = new Random();
		Integer nombreAleatoire = rand.nextInt(100000);
		numCompte = nombreAleatoire.toString();
		for (int i = 0; i <= 14 - numCompte.length(); i++) {
			numCompte = "0" + numCompte;
		}
		this.numCompte = numCompte;
		rand = new Random();
		nombreAleatoire = rand.nextInt(90) + 10;
		String cle = nombreAleatoire.toString();
		this.cle = cle;
	}

}
