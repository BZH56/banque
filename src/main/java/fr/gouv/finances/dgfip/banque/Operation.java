package fr.gouv.finances.dgfip.banque;

import java.text.DateFormat;
import java.util.Date;

public class Operation {

	protected int numOperation;
	protected Date dateOperation;
	protected String libelle;
	protected Double montant;

	DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

	public Operation(String libelle, Double montant) {
		this.libelle = libelle;
		this.montant = montant;

		// TODO: generate these fields
		this.dateOperation = new Date();
	}

	public Operation(int numOperation, String libelle, Double montant) {
		this.numOperation = numOperation;
		this.libelle = libelle;
		this.montant = montant;

		// TODO: generate these fields
		this.dateOperation = new Date();
	}

	/**
	 * @return the numOperation
	 */
	public int getNumOperation() {
		return numOperation;
	}

	/**
	 * @param numOperation the numOperation to set
	 */
	public void setNumOperation(int numOperation) {
		this.numOperation = numOperation;
	}

}
