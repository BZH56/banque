package fr.gouv.finances.dgfip.banque;

import java.util.HashSet;
import java.util.Set;

public class DonneesDAOPersonnes {

	public static int creerNouveauTitulaireautre(Set<Personne> setPersonne) {

		Personne paulette = new Personne("Blanchard".toUpperCase(), "Paulette");
		Personne dominique = new Personne("Guibert".toUpperCase(), "Dominique");
		Personne thibault = new Personne("Guillou".toUpperCase(), "Thibault");
		Personne andre = new Personne("Labbe".toUpperCase(), "André");
		Personne a0001 = new Personne("ALAN".toUpperCase(), "Alain");
		Personne a0002 = new Personne("BARDON".toUpperCase(), "Béatrice");
		Personne a0003 = new Personne("CARTON".toUpperCase(), "Charles");

		setPersonne.add(paulette);
		setPersonne.add(dominique);
		setPersonne.add(thibault);
		setPersonne.add(andre);
		setPersonne.add(a0001);
		setPersonne.add(a0002);
		setPersonne.add(a0003);
		return 1;
	}

	public static Set<Personne> creerNouveauTitulaire() {

		Set<Personne> setPersonne = new HashSet<Personne>();
		Personne paulette = new Personne("Blanchard".toUpperCase(), "Paulette");
		Personne dominique = new Personne("Guibert".toUpperCase(), "Dominique");
		Personne thibault = new Personne("Guillou".toUpperCase(), "Thibault");
		Personne andre = new Personne("Labbe".toUpperCase(), "André");
		Personne a0001 = new Personne("ALAN".toUpperCase(), "Alain");
		Personne a0002 = new Personne("BARDON".toUpperCase(), "Béatrice");
		Personne a0003 = new Personne("CARTON".toUpperCase(), "Charles");
		Personne a0004 = new Personne("DARCHE".toUpperCase(), "Denis");
		Personne a0005 = new Personne("Etan".toUpperCase(), "Esteban");
		Personne a0006 = new Personne("Forgeron".toUpperCase(), "Fred");

		setPersonne.add(paulette);
		setPersonne.add(dominique);
		setPersonne.add(thibault);
		setPersonne.add(andre);
		setPersonne.add(a0001);
		setPersonne.add(a0002);
		setPersonne.add(a0003);
		setPersonne.add(a0004);
		setPersonne.add(a0005);
		setPersonne.add(a0006);

		return setPersonne;
	}
}
