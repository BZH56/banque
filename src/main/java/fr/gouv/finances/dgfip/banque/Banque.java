package fr.gouv.finances.dgfip.banque;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.javatuples.Pair;

public class Banque implements SystemeBancaireInterface {

	protected String codeBanque;

	private HashMap<CompteBancaire, Personne> mapCompteAPersonne = new HashMap<CompteBancaire, Personne>();
	private HashMap<CarteBancaire, Pair<Personne, CompteBancaire>> mapCarteAPersonneCompteBancaire = new HashMap<CarteBancaire, Pair<Personne, CompteBancaire>>();

	public Banque(String codeBanque) {
		this.codeBanque = codeBanque;
	}

	public CompteCourant creerCompteCourant(Personne titulaire, String codeGuichet, Double soldeInitial)
			throws CompteException {
		CompteCourant cc = new CompteCourant(codeBanque, codeGuichet, soldeInitial, titulaire);
		mapCompteAPersonne.put(cc, titulaire);
		titulaire.addCompte(cc);
		return cc;
	}

	public CompteCourant creerCompteCourant(Personne titulaire, String codeGuichet) throws CompteException {
		return this.creerCompteCourant(titulaire, codeGuichet, 0.);
	}

	public CompteEpargne creerCompteEpargne(Personne titulaire, String codeGuichet, Double soldeInitial, Double taux) {
		Random rand = new Random();
		Integer nombreAleatoire = rand.nextInt(90000) + 10000;
		String numCompte = nombreAleatoire.toString();
		for (int i = 0; i <= 14 - numCompte.length(); i++) {
			numCompte = "0" + numCompte;
		}
		rand = new Random();
		nombreAleatoire = rand.nextInt(90) + 10;
		String cle = nombreAleatoire.toString();
		CompteEpargne nouveauCompteEpargne = new CompteEpargne(this.codeBanque, codeGuichet, numCompte, cle,
				soldeInitial, taux);
		mapCompteAPersonne.put(nouveauCompteEpargne, titulaire);
		return nouveauCompteEpargne;
	}

	public CarteBancaire creerCarte(Personne titulaire, CompteBancaire compte) throws CompteException {
		Random rand = new Random();
		Integer nombreAleatoire = rand.nextInt(9000) + 1000;
		String codePin = nombreAleatoire.toString();
		String numCarte = "";
		for (int i = 0; i <= 3; i++) {
			if (i != 0) {
				numCarte += " ";
			}
			nombreAleatoire = rand.nextInt(9000) + 1000;
			numCarte += nombreAleatoire.toString();
		}
		Date dateExpiration = ajouterAnnee(new Date(), 3);

		CarteBancaire CB = new CarteBancaire(codePin, numCarte, dateExpiration);
		Pair<Personne, CompteBancaire> titulaireCompte = Pair.with(titulaire, compte);
		mapCarteAPersonneCompteBancaire.put(CB, titulaireCompte);
		String paddedTitulaire = Util.padRightSpaces(titulaire.nom + " " + titulaire.prenom, 20);
		System.out.println("*** Création CB " + paddedTitulaire + " (compte courant) ***");
		return CB;

	}

	public void creerCarte() throws CompteException {

		Random rand = new Random();

		System.out.println("*********************************************************");
		for (Map.Entry<CompteBancaire, Personne> mapAParcourir : mapCompteAPersonne.entrySet()) {

			Integer ouverture = rand.nextInt(2);

			if (ouverture == 1) {
				String compteType = mapAParcourir.getKey() instanceof CompteEpargne ? "Compte epargne"
						: "Courant courant";
				if (compteType.equals("Courant courant")) {
					Personne titulaire = mapAParcourir.getValue();
					CompteBancaire compte = mapAParcourir.getKey();
					this.creerCarte(titulaire, compte);
				}
			}
		}
		System.out.println("*********************************************************\n");
	}

	public CarteBancaire lierCarte(CarteBancaire carteBancaire, CompteBancaire compte) throws CompteException {
		Personne personne = mapCompteAPersonne.get(compte);
		mapCarteAPersonneCompteBancaire.put(carteBancaire, Pair.with(personne, compte));
		return carteBancaire;
	}

	public void afficherSyntheseComptes() {

		Double soldefinal = 0.;

		System.out.println("+-----------------+--------------------------+----------------------+------------+");
		System.out.println("| Type compte     | RIB                      | Titulaire            | Solde      |");
		System.out.println("+-----------------+--------------------------+----------------------+------------+");

		for (Map.Entry<CompteBancaire, Personne> mapAParcourir : mapCompteAPersonne.entrySet()) {
//			String compteType = mapAParcourir.getKey().getClass().toString()
//					.equals("class fr.gouv.finances.dgfip.banque.CompteEpargne") ? "Compte epargne" : "Courant courant";
//			System.out.println(mapAParcourir.getKey().getClass().getSimpleName());
//			System.out.println(mapAParcourir.getKey() instanceof CompteCourant);
//			System.out.println(mapAParcourir.getKey() instanceof CompteEpargne);
			String compteType = mapAParcourir.getKey() instanceof CompteEpargne ? "Compte epargne" : "Courant courant";

//			String paddedTypeCompte = Util.padRightSpaces(compteType, 15);
//			String paddedRib = Util.padLeftSpaces(mapAParcourir.getKey().getRib(), 15);
//			String paddedTitulaire = Util
//					.padRightSpaces(mapAParcourir.getValue().nom + " " + mapAParcourir.getValue().prenom, 20);
//			soldefinal = mapAParcourir.getKey().solde;
//			for (int i = 0; i < mapAParcourir.getKey().listOperation.size(); i++) {
//				soldefinal = soldefinal + mapAParcourir.getKey().listOperation.get(i).montant;
//			}
//			String paddedSolde = Util.padLeftSpaces(soldefinal.toString(), 10);
////			System.out.println(
////					"| " + paddedTypeCompte + " | " + paddedRib + " | " + paddedTitulaire + " | " + paddedSolde + " |");

			String titulaire = mapAParcourir.getValue().nom + " " + mapAParcourir.getValue().prenom;
			System.out.println(String.format("| %-15s | %-15s | %-20s | %10.2f |", compteType,
					mapAParcourir.getKey().getRib(), titulaire, mapAParcourir.getKey().solde));
		}

		System.out.println("+-----------------+--------------------------+----------------------+------------+\n");

	}

	public void afficherSyntheseCartes() {

		System.out.println("+----------------------+----------------------+----------+");
		System.out.println("| Titulaire            | Num. Carte           | Code PIN |");
		System.out.println("+----------------------+----------------------+----------+");

		for (Map.Entry<CarteBancaire, Pair<Personne, CompteBancaire>> mapAParcourir : mapCarteAPersonneCompteBancaire
				.entrySet()) {

			Personne p = mapAParcourir.getValue().getValue0();
			String paddedTitulaire = Util.padRightSpaces(p.getNom() + " " + p.getPrenom(), 20);
			String paddedNumCarte = Util.padLeftSpaces(mapAParcourir.getKey().getNumCarte(), 20);
			String paddedCodePin = Util.padLeftSpaces(mapAParcourir.getKey().getCodePin(), 8);
			System.out.println("| " + paddedTitulaire + " | " + paddedNumCarte + " | " + paddedCodePin + " |");

		}
		System.out.println("+----------------------+----------------------+----------+");
	}

	public static Date ajouterAnnee(Date date, int nbAnnee) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.YEAR, nbAnnee);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
		date = cal.getTime();
		return date;
	}

	public List<String> rechercheRIBCompteCarte(String numCarte) throws SystemeBancaireException {
		// TODO Auto-generated method stub
		List<String> listeRIBCompteCarte = new ArrayList<String>();

		for (Map.Entry<CarteBancaire, Pair<Personne, CompteBancaire>> mapAParcourir : mapCarteAPersonneCompteBancaire
				.entrySet()) {

			CarteBancaire CB = mapAParcourir.getKey();
			String numCarteGet = CB.getNumCarte();
			if (numCarteGet.equals(numCarte)) {
				listeRIBCompteCarte.add(mapAParcourir.getValue().getValue1().codeBanque + " "
						+ mapAParcourir.getValue().getValue1().codeGuichet + " "
						+ mapAParcourir.getValue().getValue1().numCompte + " "
						+ mapAParcourir.getValue().getValue1().cle);
				// break;
			}
			// System.out.println(listeRIBCompteCarte.get(0).toString());
		}
		if (listeRIBCompteCarte.isEmpty()) {
			throw new SystemeBancaireException("Aucun compte pour ce client");
		}

		return listeRIBCompteCarte;
	}

	public Integer creerOperation(String ribCompte, String libelle, Double montant) throws SystemeBancaireException {
		// TODO Auto-generated method stub

		int numeroCourantOperation = 0;
		for (Map.Entry<CompteBancaire, Personne> mapAParcourir : mapCompteAPersonne.entrySet()) {

			CompteBancaire compteATraiter = mapAParcourir.getKey();
			String RIBCompare = compteATraiter.codeBanque + " " + compteATraiter.codeGuichet + " "
					+ compteATraiter.numCompte + " " + compteATraiter.cle;

			if (RIBCompare.equals(ribCompte)) {
				numeroCourantOperation = mapAParcourir.getKey().listOperation.size();
				Operation op = new Operation(numeroCourantOperation, libelle, -montant);
				mapAParcourir.getKey().listOperation.add(op);
			}
		}

		return numeroCourantOperation;

	}

	public Boolean verifierCodePin(String numCarteCompte, String codePin) throws SystemeBancaireException {
		// TODO Auto-generated method stub

		Boolean ok = false;
		Boolean okCarte = false;

		for (Map.Entry<CarteBancaire, Pair<Personne, CompteBancaire>> mapAParcourir : mapCarteAPersonneCompteBancaire
				.entrySet()) {

			CarteBancaire CB = mapAParcourir.getKey();
			String numCarteGet = CB.getNumCarte();
			String codePinGet = CB.getCodePin();
			if ((numCarteGet.equals(numCarteCompte)) && (codePin.equals(codePinGet))) {
				ok = true;
			}
			if (numCarteGet.equals(numCarteCompte)) {
				okCarte = true;
			}
		}
		if (!okCarte) {
			throw new SystemeBancaireException("Mauvais numéro de carte ");
		}
		return ok;
	}

	public Boolean verifierCodePinFormateur(String numCarteCompte, String codePin) throws SystemeBancaireException {
		CarteBancaire cbFound = null;

		for (Map.Entry<CarteBancaire, Pair<Personne, CompteBancaire>> mapAParcourir : mapCarteAPersonneCompteBancaire
				.entrySet()) {

			CarteBancaire CB = mapAParcourir.getKey();
			String numCarteGet = CB.getNumCarte();
			if (numCarteGet.equals(numCarteCompte)) {
				cbFound = CB;
				break;
			}
		}

		if (cbFound == null) {
			throw new SystemeBancaireException("Mauvais numéro de carte ");
		}

		return cbFound.verifierPin(codePin);
	}

	public void ajouterMouvement() throws CompteException {

		for (Map.Entry<CompteBancaire, Personne> mapAParcourir : mapCompteAPersonne.entrySet()) {
			String compteType = mapAParcourir.getKey().getClass().toString()
					.equals("class fr.gouv.finances.dgfip.banque.CompteEpargne") ? "Compte epargne" : "Courant courant";

			System.out.println("*************************************************************");

			Random rand = new Random();
			Integer nombreMouvement = rand.nextInt(10);

			for (int i = 0; i <= nombreMouvement; i++) {

				String libelleMouvement;
				Integer nombreAleatoire = rand.nextInt(10000);
				String numCompte = String.format("%04d", nombreAleatoire);
				Integer typeMouvement = rand.nextInt(2);
				Integer montantMouvement = rand.nextInt(1000);

				if (typeMouvement == 0) {
					libelleMouvement = "Débit";
					montantMouvement = -montantMouvement;
				} else {
					libelleMouvement = "Crédit";
				}
				String paddedTitulaire = Util
						.padRightSpaces(mapAParcourir.getValue().nom + " " + mapAParcourir.getValue().prenom, 20);
				String paddedTypeMouvement = Util.padRightSpaces(libelleMouvement, 6);
				String paddedMontantMouvement = Util.padLeftSpaces(montantMouvement.toString() + " €", 7);

				if (compteType.equals("Courant courant")) {
					mapAParcourir.getKey().creerOperation(libelleMouvement, Double.valueOf(montantMouvement));
				} else {
					try {
						mapAParcourir.getKey().creerOperation(libelleMouvement, Double.valueOf(montantMouvement));
					} catch (CompteException ce) {
						System.err.println("*** Impossible de débiter le compteEpargne1: " + ce.getMessage());
					}
				}

				System.out.println("*** " + paddedTypeMouvement + " de " + paddedMontantMouvement + " sur le compte  "
						+ paddedTitulaire + " ***");

			}
			System.out.println("*************************************************************\n");
		}

	}

	public void afficherSynthese() {

		for (Map.Entry<CompteBancaire, Personne> mapAParcourir : mapCompteAPersonne.entrySet()) {

			String compteType = mapAParcourir.getKey() instanceof CompteEpargne ? "Compte epargne" : "Courant courant";
			mapAParcourir.getKey().afficherSyntheseOperations(compteType, mapAParcourir.getValue().nom,
					mapAParcourir.getValue().prenom);

		}
	}

	public void afficherListeComptesOuverts(Banque banque) throws CompteException {

		List<String> compte = new ArrayList<String>();
		compte.add("compte courant");
		compte.add("compte épargne");

		Set<Personne> setPersonne = DonneesDAOPersonnes.creerNouveauTitulaire();
		for (Personne p : setPersonne) {

			Random rand = new Random();
			Integer nombreAleatoire = rand.nextInt(10000);
			String numCompte = String.format("%04d", nombreAleatoire);
			rand = new Random();
			Integer typeCompte = rand.nextInt(2);
			rand = new Random();
			Integer solde = rand.nextInt(10000);
			String paddedTitulaire = Util.padRightSpaces(p.nom + " " + p.prenom, 20);
			String paddedTypeCompte = Util.padRightSpaces(compte.get(typeCompte), 12);
			String paddedSolde = Util.padLeftSpaces(solde.toString() + " €", 7);
			System.out.println("*********************************************************");
			System.out.println("*** Ouverture d'un " + paddedTypeCompte + " " + paddedTitulaire + "***");
			if (typeCompte == 0) {
				CompteCourant cc = creerCompteCourant(p, numCompte, 0.0);
				cc.creerOperation("Dépôt chèque", Double.valueOf(solde));
				System.out.println("*** Dépôt d'un chèque de" + paddedSolde + "                       ***");
			} else {

				CompteEpargne ce = creerCompteEpargne(p, numCompte, Double.valueOf(solde), 0.02);
				System.out.println("*** montant initial = " + paddedSolde + ", taux = 2%              ***");
			}
			System.out.println("*********************************************************\n");
		}

	}

	public void afficherListeClients() throws CompteException {

		Set<Personne> setPersonne = DonneesDAOPersonnes.creerNouveauTitulaire();

		for (Personne p : setPersonne) {
			System.out.println(p.nom + " " + p.prenom);
		}

		System.out.println("");
	}
}
