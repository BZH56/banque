package fr.gouv.finances.dgfip.banque;

import java.util.Arrays;

public class CompteException extends Exception {

	@Override
	public String toString() {
		return "CompteException [getMessage()=" + getMessage() + ", getLocalizedMessage()=" + getLocalizedMessage()
				+ ", getCause()=" + getCause() + ", toString()=" + super.toString() + ", fillInStackTrace()="
				+ fillInStackTrace() + ", getStackTrace()=" + Arrays.toString(getStackTrace()) + ", getSuppressed()="
				+ Arrays.toString(getSuppressed()) + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

	public CompteException(String errorMessage) {
		super(errorMessage);
	}

}
