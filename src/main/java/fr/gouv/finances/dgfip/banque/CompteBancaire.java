package fr.gouv.finances.dgfip.banque;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CompteBancaire {

	protected String codeBanque;
	protected String codeGuichet;
	protected String numCompte;
	protected String cle;
	protected Double solde;
	protected Personne titulaire;

	List<Operation> listOperation = new ArrayList<Operation>();

	private int numeroCourantOperation = 0;

	public void afficherSyntheseOperations(String compte, String nom, String prenom) {

		Double soldefinal = 0.;

		System.out.println("Synthèse du compte: " + this.codeBanque + " " + this.codeGuichet + " " + this.numCompte
				+ " " + this.cle + " ( " + compte + " )");
		System.out.println("Titulaire: " + nom + " " + prenom);

		soldefinal = this.solde;
		String paddedMontant = Util.padLeftSpaces(soldefinal.toString(), 10);
		System.out.println("+---------+-------------------------+-------------------------+------------+");
		System.out.println("| Num opé | Date opération          | Libellé                 | Montant    |");
		System.out.println("+---------+-------------------------+-------------------------+------------+");
		System.out.println("|                                   |          Solde initial  | " + paddedMontant + " |");
		System.out.println("+---------+-------------------------+-------------------------+------------+");

		for (Operation listeOp : listOperation) {
			String paddedIndex = Util.padLeftSpaces(String.valueOf(listeOp.numOperation), 7);
			String dateCalendar = new SimpleDateFormat("dd-MM-yyyy").format(listeOp.dateOperation);
			String dateHours = new SimpleDateFormat("HH:mm:ss").format(listeOp.dateOperation);
			String date = dateCalendar + " " + dateHours;
			String paddedDate = Util.padRightSpaces(date, 23);
			String paddedLibelle = Util.padRightSpaces(listeOp.libelle, 23);
			paddedMontant = Util.padLeftSpaces(listeOp.montant.toString(), 10);
			soldefinal += listeOp.montant;
			System.out.println(
					"| " + paddedIndex + " | " + paddedDate + " | " + paddedLibelle + " | " + paddedMontant + " |");
		}
		paddedMontant = Util.padLeftSpaces(soldefinal.toString(), 10);
		System.out.println("+---------+-------------------------+-------------------------+------------+");
		System.out.println("|                                   |                 Solde   | " + paddedMontant + " |");
		System.out.println("+---------+-------------------------+-------------------------+------------+\n");

	}

	public Integer creerOperation(String libelle, Double montant) throws CompteException {
		Operation op = new Operation(libelle, montant);
		op.setNumOperation(numeroCourantOperation);
		listOperation.add(op);
		numeroCourantOperation += 1;
		return 1;
	}

	public String getRib() {
		return String.format("%s %s %10s %2s", this.codeBanque, this.codeGuichet, this.numCompte, this.cle);
	}

}
